<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m180724_062636_create_breakdown_table extends Migration
{
    /**
     * {@inheritdoc}
     */
 
    public function safeUp()
    {
        $this->createTable('breakdown', [
            'id' => $this->primaryKey(),
            'title'=> $this->string(),
            'level' => $this->integer(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('breakdown');
    }
}
