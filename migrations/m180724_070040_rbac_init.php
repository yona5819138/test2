<?php

use yii\db\Migration;

/**
 * Class m180724_070040_rbac_init
 */
class m180724_070040_rbac_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    { $auth = Yii::$app->authManager;//חובה תמיד
        
      
        $manager = $auth->createRole('manager');
        $auth->add($manager);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
   
        $member = $auth->createRole('member');
        $auth->add($member);

        $teamleader = $auth->createRole('teamleader');
        $auth->add($teamleader);

        $auth->addChild($admin, $teamleader);
        $auth->addChild($teamleader,$member);
   ////////////////////////////////////////////////////////////////////////////
        
   $manageUsers = $auth->createPermission('manageUsers');
   $auth->add($manageUsers);

   $createBreakdown = $auth->createPermission('createBreakdown');
   $auth->add($createBreakdown);

   $updateBreakdown = $auth->createPermission('updateBreakdown');
   $auth->add($updateBreakdown);

   $deleteBreakdown = $auth->createPermission('deleteBreakdown');
   $auth->add($deleteBreakdown);

   $updateLevel = $auth->createPermission('updateLevel');
   $auth->add($updateLevel);      
   
   $viewUsers = $auth->createPermission('viewUsers');
   $auth->add($viewUsers);  
   
   $viewOwnUser = $auth->createPermission('viewOwnUser');

   $rule = new \app\rbac\MemberRule;// לקרוא לזה איך שקראנו לו בתוך תיקיית rbac
   $auth->add($rule);
   
   $viewOwnUser->ruleName = $rule->name;                 
   $auth->add($viewOwnUser);                                                    
   
   $auth->addChild($admin, $manageUsers);
   $auth->addChild($admin, $viewUsers);
   $auth->addChild($member, $createBreakdown);
   $auth->addChild($member, $updateBreakdown);
   $auth->addChild($member, $deleteBreakdown);
   $auth->addChild($member, $viewOwnUser); 
   $auth->addChild($teamleader, $updateLevel);
   $auth->addChild($viewOwnUser, $viewUsers);   //קודם כל המצומצם ואז הגדול
   
   
   
   
   
   
   
   
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180724_070040_rbac_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180724_070040_rbac_init cannot be reverted.\n";

        return false;
    }
    */
}
