<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Status;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BreakdownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Breakdowns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breakdown-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Breakdown', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'level',
            [/////////////////////////////////////////////////////////////////////////////////////////////////
                'attribute'=>'status',
                'filter'=> ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'status_name')//הוספת דרופדאון בשורת חיפוש
            ],
            //עוד דרך להציג דרופדאון ליסט מבלי לקחת מהטבלה
            // [
            //     'attribute'=>'status',
            //     'filter'=>array("1"=>"open","2"=>"repair","3"=>"closed")
            // ],/////////////////////////////////////////////////////////////////////////////
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
