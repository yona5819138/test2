<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Level;
use app\models\User;
use app\models\Status;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'level')->dropDownList(
        ArrayHelper::map(Level::find()->asArray()->all(), 'id', 'level_name'), ['disabled' => !\Yii::$app->user->can('updateLevel')]   ) //למי שיש את ההרשאה הזו האופציה תהיה פתוחה בשבילו?> 
      

   <?= $form->field($model, 'status')->dropDownList(
        ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'status_name'),  ['disabled' => !\Yii::$app->user->can('updateLevel')]   ) ?>
   

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
